package com.shipper.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.animation.TranslateAnimation;

import com.shipper.R;
import com.shipper.view.activity.ActivityLogin;

/**
 * Created by PhuocDH on 11/3/2017.
 */

public class Utils {
    private static Dialog dialog;

    /**
     * show loading dialog when call API
     *
     * @param context app context
     */
    public static void showLoadingDialog(Context context) {
        if (null == context) return;
        if (dialog != null) {
            if (dialog.isShowing()) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                }
            }
            dialog = null;
        }
        dialog = new Dialog(context, android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //here we set layout of progress dialog
        dialog.setContentView(R.layout.custom_progress_dialog);
        dialog.show();
    }

    /**
     * dismiss loading dialog when call API done
     */
    public static void hideLoadingDialog() {
        try {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
                return;
            }
        } catch (Exception ex) {

        }
        if (dialog != null) {
            dialog = null;
        }
    }

    public static void invalidToken(Context context) {
        Intent intent = new Intent(context, ActivityLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();
    }
}
