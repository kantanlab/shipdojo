package com.shipper.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shipper.R;
import com.shipper.global.ConstantDefine;
import com.shipper.global.GlobalDefine;
import com.shipper.model.response.Login;
import com.shipper.presenter.PresenterLogin;
import com.shipper.utils.SharedPreferencesUtils;
import com.shipper.utils.Utils;
import com.shipper.view.iface.IActivityLogin;

import es.dmoral.toasty.Toasty;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class ActivityLogin extends ActivityBase<PresenterLogin> implements IActivityLogin {

    // UI references.
    private AutoCompleteTextView tvUserName;
    private EditText tvPassword;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        tvUserName = findViewById(R.id.tvUserName);
        tvPassword = findViewById(R.id.tvPassword);
        Login login = SharedPreferencesUtils.getInstance(mContext).getUserProfile();
        if (null != login && login.isRemember()) {
            startActivity(ActivityMain.class);
            finish();
            return;
        }

        // Set up the login form.

        tvPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    login();
                    return true;
                }
                return false;
            }
        });

        Button btLogin = findViewById(R.id.btnLogin);
        btLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    /**
     * If there are form errors (invalid user_name, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void login() {
        // Reset errors.
        tvUserName.setError(null);
        tvPassword.setError(null);

        // Store values at the time of the login attempt.
        String userName = tvUserName.getText().toString();
        String password = tvPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            tvPassword.setError(getString(R.string.error_invalid_password));
            focusView = tvPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(userName)) {
            tvUserName.setError(getString(R.string.error_field_required));
            focusView = tvUserName;
            cancel = true;
        } else if (!isUserNameValid(userName)) {
            tvUserName.setError(getString(R.string.error_invalid_user_name));
            focusView = tvUserName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            getPresenter(this).login(userName, password);
        }
    }

    private boolean isUserNameValid(String userName) {
        //TODO: Replace this with your own logic
        return userName.trim().length() > 0;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 0;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(mContext);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void loginSuccess(Login login) {
        login.setRemember(true);
        SharedPreferencesUtils.getInstance(mContext).setUserProfile(login);
        startActivity(ActivityMain.class);
        finish();
    }

    @Override
    public void loginFail(int code) {
        Utils.hideLoadingDialog();
        Toasty.error(mContext, ConstantDefine.MSG_ERROR, Toast.LENGTH_LONG).show();
    }
}

