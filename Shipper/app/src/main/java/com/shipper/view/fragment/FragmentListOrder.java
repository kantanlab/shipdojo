package com.shipper.view.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.shipper.R;
import com.shipper.global.ConstantDefine;
import com.shipper.global.GlobalDefine;
import com.shipper.model.repository.OrderRepository;
import com.shipper.model.response.DistanceResponse;
import com.shipper.model.response.GeometryLocation;
import com.shipper.model.response.LocationResponse;
import com.shipper.model.response.Order;
import com.shipper.presenter.PresenterOrder;
import com.shipper.utils.SharedPreferencesUtils;
import com.shipper.utils.Utils;
import com.shipper.view.activity.ActivityMain;
import com.shipper.view.adapter.AdapterListOrder;
import com.shipper.view.iface.IFragmentListOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.Flowable;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class FragmentListOrder extends FragmentBase<PresenterOrder> implements IFragmentListOrder {

    private SwipeRefreshLayout swListOrderSwipe;
    private RecyclerView rcvListOrderRecycler;
    private AdapterListOrder mAdapterOrder;
    private TextView tvNoData;
    private ImageView imvSort;
    private ImageView imvLogout;
    private Context mContext;
    private View mRootView;
    private List<Order> lstOrder = new ArrayList<>();
    private List<String> mListOrderId;
    private OrderRepository orderRepository;
    private String mKey;
    private boolean isSort;

    public static FragmentListOrder newInstance() {
        return new FragmentListOrder();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView != null) {
            return mRootView;
        }
        mRootView = inflater.inflate(R.layout.fragment_list_order, container, false);
        initView(mRootView);
        setAction();
        getPresenter(this).getListOrder();
        return mRootView;
    }

    private void initView(View view) {
        mContext = getContext();
        swListOrderSwipe = view.findViewById(R.id.swListOrderSwipe);
        tvNoData = view.findViewById(R.id.tvNoData);
        imvSort = view.findViewById(R.id.imvSort);
        imvLogout = view.findViewById(R.id.imvLogout);
        rcvListOrderRecycler = view.findViewById(R.id.rcvListOrderRecycler);
        rcvListOrderRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        rcvListOrderRecycler.setHasFixedSize(true);
        mAdapterOrder = new AdapterListOrder(mContext);
        rcvListOrderRecycler.setAdapter(mAdapterOrder);
        orderRepository = OrderRepository.getInstance(mContext);
        mKey = SharedPreferencesUtils.getInstance(mContext).getUserProfile().getApiLocationKey();
    }

    private void setAction() {
        swListOrderSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swListOrderSwipe.setRefreshing(true);
                getPresenter(FragmentListOrder.this).getListOrder();
            }
        });

        imvSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAdapterOrder.getItemCount() > 0) {
                    showDialogSort();
                }
            }
        });

        imvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesUtils.getInstance(getContext()).setUserProfile(null);
                Utils.invalidToken(getActivity());
            }
        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(mContext);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getListOrderSuccess(List<Order> orders) {
        lstOrder = new ArrayList<>();
        if (orders.size() == 0) {
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.GONE);
            mListOrderId = new ArrayList<>();
            List<Flowable<LocationResponse>> flowableLocation = new ArrayList<>();
            List<String> roles = SharedPreferencesUtils.getInstance(mContext).getUserProfile().getUserRoles();
            boolean isAdmin = false;
            for (String role : roles) {
                if (role.equals(GlobalDefine.ROLE_ADMIMN)) {
                    isAdmin = true;
                    break;
                } else {
                    isAdmin = false;
                }
            }

            for (Order order : orders) {
                if (!isAdmin) {
                    if (GlobalDefine.PROCESSING.equals(order.getStatus()) ||
                            GlobalDefine.PENDING.equals(order.getStatus()) ||
                            GlobalDefine.ON_HOLD.equals(order.getStatus())) {
                    } else {
                        lstOrder.add(order);
                        String address = order.getShipping().getAddress1();
                        if (address.isEmpty()) {
                            address = order.getShipping().getAddress2();
                        }
                        if (!address.isEmpty()) {
                            mListOrderId.add(order.getNumber());
                            Flowable<LocationResponse> flowable = orderRepository.getLocation(address, mKey);
                            flowableLocation.add(flowable);
                        }
                    }
                } else {
                    if (!GlobalDefine.ON_HOLD.equals(order.getStatus())) {
                        lstOrder.add(order);
                        String address = order.getShipping().getAddress1();
                        if (address.isEmpty()) {
                            address = order.getShipping().getAddress2();
                        }
                        if (!address.isEmpty()) {
                            mListOrderId.add(order.getNumber());
                            Flowable<LocationResponse> flowable = orderRepository.getLocation(address, mKey);
                            flowableLocation.add(flowable);
                        }
                    }
                }
            }

            mAdapterOrder.setData(lstOrder);
            if (flowableLocation.size() != 0) {
                getPresenter(this).getLocation(flowableLocation, mListOrderId, mKey);
            }
        }
        swListOrderSwipe.setRefreshing(false);
    }

    @Override
    public void getListOrderFail(int code) {
        Toasty.error(getContext(), ConstantDefine.MSG_ERROR, Toast.LENGTH_LONG).show();
        tvNoData.setVisibility(View.VISIBLE);
        swListOrderSwipe.setRefreshing(false);
    }

    @Override
    public void getLocationSuccess(LocationResponse locationResponse, String orderId) {

        for (Order order : lstOrder) {
            if (order.getNumber().equals(orderId)) {
                if (locationResponse.getLocation().size() != 0) {
                    order.setGeometryLocationEnd(locationResponse.getLocation().get(0).getGeometry().getGeometryLocation());
                    Log.d("updateLocation", " getLocationSuccess " + orderId);
                }
                return;
            }
        }
    }

    @Override
    public void getLocationComplete(List<Flowable<DistanceResponse>> flowableDistance, List<String> listOrderId) {
        getPresenter(this).getDistance2Location(flowableDistance, listOrderId);
    }

    @Override
    public void getLocationFail(int code) {
        Toasty.error(getContext(), ConstantDefine.MSG_ERROR, Toast.LENGTH_LONG).show();
    }

    @Override
    public void getDistance2LocationSuccess(DistanceResponse distanceResponse, String orderId) {

        for (Order order : lstOrder) {
            if (order.getNumber().equals(orderId)) {
                Log.d("updateLocation", " getDistance2LocationSuccess " + orderId + " -- " + distanceResponse.getRoutes().size());
                if (distanceResponse.getRoutes().size() != 0) {
                    String distance = distanceResponse.getRoutes().get(0).getLegs().get(0).getDistance().getText();
                    long distanceValue = distanceResponse.getRoutes().get(0).getLegs().get(0).getDistance().getValue();
                    String duration = distanceResponse.getRoutes().get(0).getLegs().get(0).getDuration().getText();
                    order.setDistance(distance);
                    order.setDistanceValue(distanceValue);
                    order.setDuration(duration);
                    GeometryLocation geometryLocation = new GeometryLocation();
                    geometryLocation.setLat(((ActivityMain) getContext()).getLocation().getLatitude());
                    geometryLocation.setLng(((ActivityMain) getContext()).getLocation().getLongitude());
                    order.setGeometryLocationCurrent(geometryLocation);
                    mAdapterOrder.setData(lstOrder);
                }
                return;
            }
        }
    }

    @Override
    public void getDistance2LocationFail(int code) {
        Toasty.error(getContext(), ConstantDefine.MSG_ERROR, Toast.LENGTH_LONG).show();
    }

    public void updateLocation(Location currentLocation) {
        List<Flowable<DistanceResponse>> flowableDistance = new ArrayList<>();
        List<String> listOrderId = new ArrayList<>();
        for (Order order : lstOrder) {
            if (null != order.getGeometryLocationEnd()) {
                Location location = new Location("endLocation");
                location.setLatitude(order.getGeometryLocationEnd().getLat());
                location.setLongitude(order.getGeometryLocationEnd().getLng());
                GeometryLocation geometryLocation = new GeometryLocation();
                geometryLocation.setLat(currentLocation.getLatitude());
                geometryLocation.setLng(currentLocation.getLongitude());
                order.setGeometryLocationCurrent(geometryLocation);
                Log.d("updateLocation", " updateLocation " + order.getNumber());
                listOrderId.add(order.getNumber());
                Flowable<DistanceResponse> flowable = orderRepository.getDistance2Location(order.getGeometryLocationEnd().getLat() + "," + order.getGeometryLocationEnd().getLng(),
                        currentLocation.getLatitude() + "," + currentLocation.getLongitude(), mKey);
                flowableDistance.add(flowable);
            }
        }
        if (flowableDistance.size() > 0) {
            getPresenter(this).getDistance2Location(flowableDistance, listOrderId);
        }
    }

    public void updateStatus() {
        int size = lstOrder.size();
        int position = ((ActivityMain) mContext).getPosition();
        String status = ((ActivityMain) mContext).getStatus();

        for (int i = 0; i < size; i++) {
            if (i == position) {
                if (null != status) {
                    lstOrder.get(i).setStatus(status);
                    mAdapterOrder.setData(lstOrder);
                }
                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateStatus();
    }

    public void showDialogSort() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_sort_list_order, null);
        dialogBuilder.setView(dialog);
        final RadioButton rdDistance = dialog.findViewById(R.id.rdDistance);
        final LinearLayout llStatus = dialog.findViewById(R.id.llStatus);
        final CheckBox cbPending = dialog.findViewById(R.id.cbPending);
        final CheckBox cbProcessing = dialog.findViewById(R.id.cbProcessing);
        final CheckBox cbCompleted = dialog.findViewById(R.id.cbCompleted);
        final CheckBox cbShipperOnHand = dialog.findViewById(R.id.cbShipperOnHand);
        final CheckBox cbCustomerOnHand = dialog.findViewById(R.id.cbCustomerOnHand);

        rdDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (rdDistance.isChecked()) {
                    llStatus.setVisibility(View.GONE);
                } else {
                    llStatus.setVisibility(View.VISIBLE);
                }
            }
        });

        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (rdDistance.isChecked()) {
                    Collections.sort(mAdapterOrder.mListOrder, new Order.CompDistance());
                    mAdapterOrder.notifyDataSetChanged();
                } else {
                    List<String> lst = new ArrayList<>();
                    if (cbPending.isChecked()) {
                        lst.add(GlobalDefine.PENDING);
                    }
                    if (cbProcessing.isChecked()) {
                        lst.add(GlobalDefine.PROCESSING);
                    }
                    if (cbCompleted.isChecked()) {
                        lst.add(GlobalDefine.COMPLETED);
                    }
                    if (cbShipperOnHand.isChecked()) {
                        lst.add(GlobalDefine.SHIPPER_ON_HAND);
                    }
                    if (cbCustomerOnHand.isChecked()) {
                        lst.add(GlobalDefine.CUSTOMER_ON_HAND);
                    }
                    mAdapterOrder.filter(lst);
                }
                dialogInterface.dismiss();
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialogBuilder.create();
        dialogBuilder.show();
    }
}
