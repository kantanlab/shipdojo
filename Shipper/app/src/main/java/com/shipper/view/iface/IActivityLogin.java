package com.shipper.view.iface;

import com.shipper.model.response.Login;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public interface IActivityLogin extends IViewBase {

    void loginSuccess(Login login);

    void loginFail(int code);

}
