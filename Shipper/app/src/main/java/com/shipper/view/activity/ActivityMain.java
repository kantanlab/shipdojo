package com.shipper.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.shipper.R;
import com.shipper.view.fragment.FragmentListOrder;

import es.dmoral.toasty.Toasty;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class ActivityMain extends ActivityBase {

    private static final int ACCESS_LOCATION = 1518;
    private boolean doubleBackToExitPressedOnce = false;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private boolean isFirstRequest = false;
    private int mPosition;
    private String mStatus;
    private Location mLocation;
    private FragmentListOrder fragmentListOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentListOrder = FragmentListOrder.newInstance();
        replaceFragmentWithAnimation(fragmentListOrder, false);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (mLocation == null) {
                    fragmentListOrder.updateLocation(location);
                } else {
                    if (mLocation.distanceTo(location) > 300) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();
                        if (mLocation != null) {
                            fragmentListOrder.updateLocation(location);
                            Log.d("onLocationChanged", latitude + " -- " + longitude + " -- " + mLocation.distanceTo(location));
                        }
                    }
                }
                mLocation = location;
                Log.d("onLocationChanged ", "onLocationChanged -- " + " -- " + mLocation);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.d("onLocationChanged ", "onStatusChanged " + s + " -- " + i);
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d("onLocationChanged ", "onProviderEnabled " + s);
            }

            @Override
            public void onProviderDisabled(String s) {
                Log.d("onLocationChanged ", "onProviderDisabled " + s);
            }
        };

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACCESS_LOCATION) {
            requestPermission();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toasty.info(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    ACCESS_LOCATION);
        } else {
            mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
            if (mLocation == null) {
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
            }

            if (mLocation != null) {
                fragmentListOrder.updateLocation(mLocation);
            }
        }
    }

    private void requestLocation() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                && !mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && !isFirstRequest) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Location Permission");
            builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    isFirstRequest = false;
                }
            });
            builder.setCancelable(false);
            builder.show();
            isFirstRequest = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPermission();
        requestLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mLocationManager.removeUpdates(mLocationListener);
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int mPosition) {
        this.mPosition = mPosition;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public Location getLocation() {
        return mLocation;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocation = null;
        mLocationListener = null;
        mLocationManager = null;
    }
}
