package com.shipper.view.iface;

import com.shipper.model.response.Order;

import java.util.List;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public interface IFragmentListOrderDetail extends IViewBase {

    void getListOrderDetailSuccess(Order order);

    void doConfirmShippingSuccess(Order order);

    void getListOrderDetailFail(int code);
}
