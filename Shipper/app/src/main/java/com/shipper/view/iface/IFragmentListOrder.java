package com.shipper.view.iface;

import com.shipper.model.response.DistanceResponse;
import com.shipper.model.response.LocationResponse;
import com.shipper.model.response.Order;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public interface IFragmentListOrder extends IViewBase {

    void getListOrderSuccess(List<Order> order);

    void getListOrderFail(int code);

    void getLocationSuccess(LocationResponse locationResponse, String orderId);

    void getLocationComplete(List<Flowable<DistanceResponse>> flowableDistance,  List<String> listOrderId);

    void getLocationFail(int code);

    void getDistance2LocationSuccess(DistanceResponse distanceResponse, String orderId);

    void getDistance2LocationFail(int code);
}
