package com.shipper.view.iface;

import android.content.Context;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public interface IViewBase {

    Context getContext();

    void showLoading();

    void hideLoading();
}
