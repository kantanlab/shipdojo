package com.shipper.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shipper.R;
import com.shipper.model.response.LineItem;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class AdapterListOrderDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<LineItem> mListOrderDetail;
    private Context mContext;
    private String mCurrency;

    public AdapterListOrderDetail(Context context) {
        mContext = context;
    }

    public void setData(List<LineItem> listOrderDetail, String currency) {
        mListOrderDetail = listOrderDetail;
        mCurrency = currency;
    }

    public class OrderDetailHolder extends RecyclerView.ViewHolder {

        private TextView tvNumberOrder;
        private TextView tvQuantity;
        private TextView tvName;
        private TextView tvPrice;
        private TextView tvSum;

        public OrderDetailHolder(View itemView) {
            super(itemView);
            tvNumberOrder = itemView.findViewById(R.id.tvNumberOrder);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvName = itemView.findViewById(R.id.tvName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvSum = itemView.findViewById(R.id.tvSum);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_order_detail, parent, false);
        return new OrderDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final LineItem orderDetail = mListOrderDetail.get(position);
        OrderDetailHolder orderDetailHolder = (OrderDetailHolder) holder;
        orderDetailHolder.tvNumberOrder.setText((position + 1) + ")");
        orderDetailHolder.tvQuantity.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Long.valueOf(orderDetail.getQuantity())) + "");
        orderDetailHolder.tvName.setText(orderDetail.getName());
        orderDetailHolder.tvPrice.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Long.valueOf(orderDetail.getPrice())) + " " + mCurrency + " = ");
        orderDetailHolder.tvSum.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Long.valueOf(orderDetail.getTotal())) + " đ");
    }

    @Override
    public int getItemCount() {
        return mListOrderDetail != null ? mListOrderDetail.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
