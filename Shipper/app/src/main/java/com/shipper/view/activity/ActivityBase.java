package com.shipper.view.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.shipper.R;
import com.shipper.presenter.PresenterBase;
import com.shipper.view.fragment.FragmentBase;
import com.shipper.view.iface.IViewBase;

import java.lang.reflect.ParameterizedType;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class ActivityBase<P extends PresenterBase> extends AppCompatActivity {

    private P viewPresenter;

    public P getPresenter(IViewBase iViewBase) {
        try {
            if (this.viewPresenter == null) {
                String e = ((Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getName();
                Class classDefinition = Class.forName(e);
                this.viewPresenter = (P) classDefinition.newInstance();
                this.viewPresenter.setIFace(iViewBase);
                this.viewPresenter.onInit();
                return this.viewPresenter;
            }
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        } catch (ClassNotFoundException var6) {
            var6.printStackTrace();
        }

        return this.viewPresenter;
    }

    public void startActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    public void replaceFragmentWithAnimation(FragmentBase fragment, boolean addToBackStack) {
        String TAB = fragment.getClass().getName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.view_enter_from_left, R.anim.view_exit_to_right, R.anim.view_enter_from_right, R.anim.view_exit_to_left);
        if (addToBackStack) {
            transaction.addToBackStack(TAB);
        }
        transaction.replace(R.id.content, fragment, TAB);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    public boolean popFragment() {
        if (getFragmentManager().getBackStackEntryCount() >= 0) {
            getFragmentManager().popBackStackImmediate();
            return true;
        }
        return false;
    }
}
