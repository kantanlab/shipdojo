package com.shipper.view.fragment;

import android.graphics.Color;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shipper.R;
import com.shipper.utils.SharedPreferencesUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by PhuocDH on 11/23/2017.
 */

public class FragmentMap extends FragmentBase implements OnMapReadyCallback, View.OnClickListener {

    private LatLng mLatLngCurrent;
    private LatLng mLatLngEnd;
    private String[] colors = {"#7fff7272", "#7f31c7c5", "#7fff8a00"};
    private GoogleMap mGoogleMap;
    private TextView tvMapTypeNormal;
    private TextView tvMapTypeHybrid;
    private String mAddressShipper;
    private String mAddressSeller;
    private String mAddressConsignee;

    public static FragmentMap newInstance() {
        return new FragmentMap();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLatLngCurrent = getArguments().getParcelable(FragmentOrderDetail.LATLNG_CURRENT);
        mLatLngEnd = getArguments().getParcelable(FragmentOrderDetail.LATLNG_END);
        mAddressSeller = getArguments().getString(FragmentOrderDetail.ADDRESS_SELLER);
        mAddressConsignee = getArguments().getString(FragmentOrderDetail.ADDRESS_CONSIGNEE);
        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            mAddressShipper = geocoder.getFromLocation(mLatLngCurrent.latitude, mLatLngCurrent.longitude, 1).get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = new SupportMapFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.mMapView, mapFragment).addToBackStack(FragmentMap.class.getName())
                .commit();
        tvMapTypeNormal = v.findViewById(R.id.tvMapTypeNormal);
        tvMapTypeHybrid = v.findViewById(R.id.tvMapTypeHybrid);
        mapFragment.getMapAsync(this);
        tvMapTypeNormal.setOnClickListener(this);
        tvMapTypeHybrid.setOnClickListener(this);
        handleBackPress(v);
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvMapTypeNormal:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.tvMapTypeHybrid:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        String mKey = SharedPreferencesUtils.getInstance(getContext()).getUserProfile().getApiLocationKey();
        GoogleDirection.withServerKey(mKey)
                .from(mLatLngCurrent)
                .to(mLatLngEnd)
                .avoid(AvoidType.FERRIES)
                .avoid(AvoidType.HIGHWAYS)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        if (direction.isOK()) {
                            for (int i = 0; i < direction.getRouteList().size(); i++) {
                                Route route = direction.getRouteList().get(i);
                                String color = colors[i % colors.length];
                                ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
                                mGoogleMap.addPolyline(DirectionConverter.createPolyline(getContext(), directionPositionList, 5, Color.parseColor(color)));
                            }
                            setCameraWithCoordinationBounds(direction.getRouteList().get(0));
                            mGoogleMap.addMarker(new MarkerOptions().position(mLatLngCurrent).title(mAddressShipper)
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_my_location))).showInfoWindow();
                            mGoogleMap.addMarker(new MarkerOptions().position(mLatLngEnd).title(mAddressConsignee)
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location_consignee))).showInfoWindow();
                        } else {
                            // Do something
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something
                    }
                });
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    private void setCameraWithCoordinationBounds(Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }
}
