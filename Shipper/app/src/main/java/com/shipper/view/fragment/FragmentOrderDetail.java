package com.shipper.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.shipper.R;
import com.shipper.global.ConstantDefine;
import com.shipper.global.GlobalDefine;
import com.shipper.model.response.Order;
import com.shipper.presenter.PresenterOrderDetail;
import com.shipper.utils.Utils;
import com.shipper.view.activity.ActivityMain;
import com.shipper.view.adapter.AdapterListOrderDetail;
import com.shipper.view.iface.IFragmentListOrderDetail;

import java.text.NumberFormat;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class FragmentOrderDetail extends FragmentBase<PresenterOrderDetail> implements IFragmentListOrderDetail, View.OnClickListener {

    public static final String ORDER_ID = "order_id";
    public static final String LATLNG_CURRENT = "latlng_current";
    public static final String LATLNG_END = "latlng_end";
    public static final String ADDRESS_SELLER = "address_seller";
    public static final String ADDRESS_CONSIGNEE = "address_consignee";
    private RecyclerView rcvListOrderDetailRecycler;
    private AdapterListOrderDetail mAdapterOrderDetail;
    private TextView tvOrderId;
    private TextView tvShipStatus;
    private TextView tvSeller;
    private TextView tvTelSeller;
    private TextView tvAddressSeller;
    private TextView tvConsignee;
    private TextView tvTelConsignee;
    private TextView tvAddressConsignee;
    private TextView tvSubTotal;
    private TextView tvShippingFee;
    private TextView tvTotal;
    private Button btShippingStatus;
    private Button btConfirmReceived;
    private ImageView imvIconBack;
    private ImageView imvIconMap;
    private LinearLayout llLayoutHeader;
    private Context mContext;
    private String mOrderId;
    private LatLng mLatLngCurrent;
    private LatLng mLatLngEnd;
    private View mRootView;
    private String mAddressSeller;
    private String mAddressConsignee;

    public static FragmentOrderDetail newInstance() {
        return new FragmentOrderDetail();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mOrderId = getArguments().getString(ORDER_ID);
        mLatLngCurrent = getArguments().getParcelable(LATLNG_CURRENT);
        mLatLngEnd = getArguments().getParcelable(LATLNG_END);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView != null) {
            handleBackPress(mRootView);
            return mRootView;
        }
        mRootView = inflater.inflate(R.layout.fragment_list_order_detail, container, false);
        initView(mRootView);
        setAction();
        handleBackPress(mRootView);
        return mRootView;
    }

    private void initView(View view) {
        mContext = getContext();
        ((ActivityMain) mContext).setTitle(mContext.getResources().getString(R.string.order_detail));
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvShipStatus = view.findViewById(R.id.tvShipStatus);
        tvSeller = view.findViewById(R.id.tvSeller);
        tvTelSeller = view.findViewById(R.id.tvTelSeller);
        tvAddressSeller = view.findViewById(R.id.tvAddressSeller);
        tvConsignee = view.findViewById(R.id.tvConsignee);
        tvTelConsignee = view.findViewById(R.id.tvTelConsignee);
        tvAddressConsignee = view.findViewById(R.id.tvAddressConsignee);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        tvShippingFee = view.findViewById(R.id.tvShippingFee);
        tvTotal = view.findViewById(R.id.tvTotal);
        btShippingStatus = view.findViewById(R.id.btShippingStatus);
        btConfirmReceived = view.findViewById(R.id.btConfirmReceived);
        imvIconBack = view.findViewById(R.id.imvIconBack);
        imvIconMap = view.findViewById(R.id.imvIconMap);
        llLayoutHeader = view.findViewById(R.id.llLayoutHeader);
        rcvListOrderDetailRecycler = view.findViewById(R.id.rcvListOrderDetailRecycler);
        rcvListOrderDetailRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        rcvListOrderDetailRecycler.setHasFixedSize(true);
        mAdapterOrderDetail = new AdapterListOrderDetail(mContext);
        rcvListOrderDetailRecycler.setAdapter(mAdapterOrderDetail);
    }

    private void setAction() {
        btShippingStatus.setOnClickListener(this);
        btConfirmReceived.setOnClickListener(this);
        tvTelSeller.setOnClickListener(this);
        tvTelConsignee.setOnClickListener(this);
        imvIconBack.setOnClickListener(this);
        imvIconMap.setOnClickListener(this);
        getPresenter(this).getListOrderDetail(mOrderId);
    }

    private void setData(Order order) {
        if (mLatLngCurrent == null || mLatLngEnd == null) {
            imvIconMap.setBackground(mContext.getResources().getDrawable(R.drawable.ic_location_off_24dp));
            imvIconMap.setEnabled(false);
        } else {
            imvIconMap.setBackground(mContext.getResources().getDrawable(R.drawable.ic_location_on_24dp));
            imvIconMap.setEnabled(true);
        }
        tvTelSeller.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvTelConsignee.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        setColor(order.getStatus());
        if (order.getStatus().equals(GlobalDefine.SHIPPER_ON_HAND)) {
            btShippingStatus.setText(mContext.getString(R.string.bt_shipping));
        } else {
            btShippingStatus.setText(mContext.getString(R.string.bt_shipping_status));
        }
        tvOrderId.setText(order.getNumber());
        tvSeller.setText(order.getBilling().getFirstName() + " " + order.getShipping().getLastName());
        tvTelSeller.setText(order.getBilling().getPhone());
        mAddressSeller = order.getBilling().getAddress1();
        tvAddressSeller.setText(mAddressSeller);
        tvConsignee.setText(order.getShipping().getFirstName() + " " + order.getShipping().getLastName());
        tvTelConsignee.setText(order.getShipping().getPhone());
        mAddressConsignee = order.getShipping().getAddress1();
        tvAddressConsignee.setText(mAddressConsignee);
        double subTotal = Double.valueOf(order.getTotal()) - Double.valueOf(order.getShippingTotal());
        tvSubTotal.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.valueOf(subTotal)) + " đ");
        tvShippingFee.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.valueOf(order.getShippingTotal())) + " đ");
        tvTotal.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.valueOf(order.getTotal())) + " đ");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imvIconBack:
                popFragment();
                break;
            case R.id.btShippingStatus:
                if (btShippingStatus.getText().equals(mContext.getString(R.string.bt_shipping))) {
                    getPresenter(this).doConfirmShipping(GlobalDefine.COMPLETED, mOrderId);
                } else {
                    getPresenter(this).doConfirmShipping(GlobalDefine.SHIPPER_ON_HAND, mOrderId);
                }
                break;
            case R.id.btConfirmReceived:
                getPresenter(this).doConfirmShipping(GlobalDefine.CUSTOMER_ON_HAND, mOrderId);
                break;
            case R.id.tvTelSeller:
                Intent intentTelSeller = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + tvTelSeller.getText()));
                mContext.startActivity(intentTelSeller);
                break;
            case R.id.tvTelConsignee:
                Intent intentConsignee = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + tvTelConsignee.getText()));
                mContext.startActivity(intentConsignee);
                break;
            case R.id.imvIconMap:
                FragmentMap fragmentMap = FragmentMap.newInstance();
                Bundle bundle = new Bundle();
                bundle.putParcelable(LATLNG_CURRENT, mLatLngCurrent);
                bundle.putParcelable(LATLNG_END, mLatLngEnd);
                bundle.putString(ADDRESS_SELLER, mAddressSeller);
                bundle.putString(ADDRESS_CONSIGNEE, mAddressConsignee);
                fragmentMap.setArguments(bundle);
                ((ActivityMain) mContext).replaceFragmentWithAnimation(fragmentMap, true);
                break;
        }
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(mContext);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getListOrderDetailSuccess(Order order) {
        setData(order);
        mAdapterOrderDetail.setData(order.getLineItems(), order.getCurrency());
    }

    @Override
    public void doConfirmShippingSuccess(Order order) {
        tvShipStatus.setText(order.getStatus());
        setColor(order.getStatus());
        if (order.getStatus().equals(GlobalDefine.SHIPPER_ON_HAND)) {
            btShippingStatus.setText(mContext.getString(R.string.bt_shipping));
        } else {
            btShippingStatus.setText(mContext.getString(R.string.bt_shipping_status));
        }
        ((ActivityMain) mContext).setStatus(order.getStatus());
    }

    @Override
    public void getListOrderDetailFail(int code) {
        Toasty.error(getContext(), ConstantDefine.MSG_ERROR, Toast.LENGTH_LONG).show();
    }

    private void setColor(String status) {
        btConfirmReceived.setEnabled(true);
        btShippingStatus.setEnabled(true);
        if (GlobalDefine.COMPLETED.equals(status)) {
            llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_orange_dark));
            tvShipStatus.setText(mContext.getString(R.string.completed));
        } else if (GlobalDefine.PROCESSING.equals(status)) {
            llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_blue_bright));
            tvShipStatus.setText(mContext.getString(R.string.processing));
        } else if (GlobalDefine.SHIPPER_ON_HAND.equals(status)) {
            llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_purple));
            tvShipStatus.setText(mContext.getString(R.string.shipper_on_hand));
        } else if (GlobalDefine.CUSTOMER_ON_HAND.equals(status)) {
            llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_green_dark));
            tvShipStatus.setText(mContext.getString(R.string.customer_on_hand));
        } else if (GlobalDefine.KEEPER_ON_HAND.equals(status)) {
            llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.tab_indicator_text));
            tvShipStatus.setText(mContext.getString(R.string.keeper_on_hand));
        } else {
            llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_light));
            tvShipStatus.setText(mContext.getString(R.string.pending));
        }
    }
}
