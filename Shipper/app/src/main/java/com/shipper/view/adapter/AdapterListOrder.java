package com.shipper.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.shipper.R;
import com.shipper.global.GlobalDefine;
import com.shipper.model.response.Order;
import com.shipper.utils.SharedPreferencesUtils;
import com.shipper.view.activity.ActivityMain;
import com.shipper.view.fragment.FragmentOrderDetail;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class AdapterListOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Order> mListOrder;
    private List<Order> mListOrderTemp;
    private Context mContext;

    public AdapterListOrder(Context context) {
        mContext = context;
    }

    public void setData(List<Order> listOrder) {
        mListOrder = listOrder;
        mListOrderTemp = new ArrayList<>();
        mListOrderTemp.addAll(listOrder);
        notifyDataSetChanged();
    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imvIconDown;
        private TextView tvOrderId;
        private TextView tvShipStatus;
        private TextView tvConsignee;
        private TextView tvTel;
        private TextView tvAddress;
        private TextView tvTotal;
        private TextView tvDistance;
        private TextView tvDuration;
        private Button btDetail;
        private LinearLayout llLayoutHeader;
        private LinearLayout llLayoutDetail;

        public OrderHolder(View itemView) {
            super(itemView);
            imvIconDown = itemView.findViewById(R.id.imvIconDown);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvShipStatus = itemView.findViewById(R.id.tvShipStatus);
            tvConsignee = itemView.findViewById(R.id.tvConsignee);
            tvTel = itemView.findViewById(R.id.tvTel);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvTotal = itemView.findViewById(R.id.tvTotal);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            tvDuration = itemView.findViewById(R.id.tvDuration);
            btDetail = itemView.findViewById(R.id.btDetail);
            llLayoutHeader = itemView.findViewById(R.id.llLayoutHeader);
            llLayoutDetail = itemView.findViewById(R.id.llLayoutDetail);
            tvTel.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            llLayoutHeader.setOnClickListener(this);
            tvTel.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.llLayoutHeader:
                    if (llLayoutDetail.getVisibility() != View.GONE) {
                        llLayoutDetail.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_up));
                        CountDownTimer countDownTimerStatic = new CountDownTimer(500, 16) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                imvIconDown.setBackground(mContext.getResources().getDrawable(R.drawable.ic_arrow_down_black_24dp));
                                llLayoutDetail.setVisibility(View.GONE);
                            }
                        };
                        countDownTimerStatic.start();
                    } else {
                        imvIconDown.setBackground(mContext.getResources().getDrawable(R.drawable.ic_arrow_up_black_24dp));
                        llLayoutDetail.setVisibility(View.VISIBLE);
                        llLayoutDetail.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_down));
                    }
                    break;
                case R.id.tvTel:
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + tvTel.getText()));
                    mContext.startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_order, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Order order = mListOrder.get(position);
        OrderHolder orderHolder = (OrderHolder) holder;

        if (GlobalDefine.COMPLETED.equals(order.getStatus())) {
            orderHolder.llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_orange_dark));
            orderHolder.tvShipStatus.setText(mContext.getString(R.string.completed));
        } else if (GlobalDefine.PROCESSING.equals(order.getStatus())) {
            orderHolder.llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_blue_bright));
            orderHolder.tvShipStatus.setText(mContext.getString(R.string.processing));
        } else if (GlobalDefine.SHIPPER_ON_HAND.equals(order.getStatus())) {
            orderHolder.llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_purple));
            orderHolder.tvShipStatus.setText(mContext.getString(R.string.shipper_on_hand));
        } else if (GlobalDefine.CUSTOMER_ON_HAND.equals(order.getStatus())) {
            orderHolder.llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_green_dark));
            orderHolder.tvShipStatus.setText(mContext.getString(R.string.customer_on_hand));
        } else if (GlobalDefine.KEEPER_ON_HAND.equals(order.getStatus())) {
            orderHolder.llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.tab_indicator_text));
            orderHolder.tvShipStatus.setText(mContext.getString(R.string.keeper_on_hand));
        } else {
            orderHolder.llLayoutHeader.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_light));
            orderHolder.tvShipStatus.setText(mContext.getString(R.string.pending));
        }
        orderHolder.tvOrderId.setText(order.getNumber());
        orderHolder.tvConsignee.setText(order.getShipping().getFirstName() + " " + order.getShipping().getLastName());
        orderHolder.tvTel.setText(order.getShipping().getPhone());
        orderHolder.tvAddress.setText(order.getShipping().getAddress1());
        orderHolder.tvTotal.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.valueOf(order.getTotal())) + " đ");

        if (order.getDistance() == null) {
            orderHolder.tvDistance.setText(mContext.getResources().getString(R.string.unknow));
            orderHolder.tvDuration.setVisibility(View.GONE);
        } else {
            orderHolder.tvDistance.setText(order.getDistance());
            orderHolder.tvDuration.setVisibility(View.VISIBLE);
            orderHolder.tvDuration.setText(order.getDuration());
        }

        orderHolder.btDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentOrderDetail fragmentOrderDetail = FragmentOrderDetail.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(FragmentOrderDetail.ORDER_ID, order.getNumber());
                if (null != order.getGeometryLocationCurrent()) {
                    LatLng latLngCurrent = new LatLng(order.getGeometryLocationCurrent().getLat(), order.getGeometryLocationCurrent().getLng());
                    LatLng latLngEnd = new LatLng(order.getGeometryLocationEnd().getLat(), order.getGeometryLocationEnd().getLng());
                    bundle.putParcelable(FragmentOrderDetail.LATLNG_CURRENT, latLngCurrent);
                    bundle.putParcelable(FragmentOrderDetail.LATLNG_END, latLngEnd);
                }
                fragmentOrderDetail.setArguments(bundle);
                ((ActivityMain) mContext).replaceFragmentWithAnimation(fragmentOrderDetail, true);
                ((ActivityMain) mContext).setPosition(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListOrder != null ? mListOrder.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void filter(List<String> lstStatus) {
        if (lstStatus.size() == 0) {
            return;
        }
        List<Order> lstOrderFilter = new ArrayList<>();
        for (Order order : mListOrderTemp) {
            for (String str : lstStatus) {
                if (str.equals(order.getStatus())) {
                    lstOrderFilter.add(order);
                }
            }
        }
        mListOrder = lstOrderFilter;
        notifyDataSetChanged();
    }
}
