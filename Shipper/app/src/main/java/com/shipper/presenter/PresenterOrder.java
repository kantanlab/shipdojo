package com.shipper.presenter;

import android.location.Location;
import android.util.Log;

import com.shipper.model.net.DataCallBack;
import com.shipper.model.repository.OrderRepository;
import com.shipper.model.response.DistanceResponse;
import com.shipper.model.response.LocationResponse;
import com.shipper.model.response.Order;
import com.shipper.view.activity.ActivityMain;
import com.shipper.view.iface.IFragmentListOrder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.schedulers.IoScheduler;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class PresenterOrder extends PresenterBase<IFragmentListOrder> {
    private OrderRepository orderRepository;
    private int mPosition = 0;

    @Override
    public void onInit() {
        super.onInit();
        orderRepository = OrderRepository.getInstance(getIFace().getContext());
    }

    public void getListOrder() {
        getIFace().showLoading();
        orderRepository.getListOrder(new DataCallBack<List<Order>>() {
            @Override
            public void onSuccess(List<Order> result) {
                getIFace().hideLoading();
                getIFace().getListOrderSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().getListOrderFail(errorCode);
                getIFace().hideLoading();
            }
        });
    }

    public void getLocation(List<Flowable<LocationResponse>> flowableLocation, final List<String> listOrderId, final String key) {
        mPosition = 0;
        final List<String> listOrderIdTmp = new ArrayList<>();
        final List<Flowable<DistanceResponse>> flowableDistance = new ArrayList<>();
        Flowable.concat(flowableLocation)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(new IoScheduler())
                .subscribe(new DisposableSubscriber<LocationResponse>() {
                    @Override
                    public void onNext(LocationResponse locationResponse) {
                        getIFace().getLocationSuccess(locationResponse, listOrderId.get(mPosition));
                        Location currentLocation = ((ActivityMain) getContext()).getLocation();
                        if (null != currentLocation && locationResponse.getStatus().equals("OK")) {
                            listOrderIdTmp.add(listOrderId.get(mPosition));
                            Flowable<DistanceResponse> flowable = orderRepository.getDistance2Location(
                                    locationResponse.getLocation().get(0).getGeometry().getGeometryLocation().getLat() + "," +
                                            locationResponse.getLocation().get(0).getGeometry().getGeometryLocation().getLng(),
                                    currentLocation.getLatitude() + "," +
                                            currentLocation.getLongitude(), key);
                            flowableDistance.add(flowable);
                        }
                        mPosition = mPosition + 1;
                    }

                    @Override
                    public void onError(Throwable t) {
                        getIFace().getLocationFail(t.hashCode());
                        mPosition = mPosition + 1;
                        Log.d("updateLocation", " getLocation_onError " + mPosition);
                    }

                    @Override
                    public void onComplete() {
                        getIFace().getLocationComplete(flowableDistance, listOrderIdTmp);
                    }
                });
    }

    public void getDistance2Location(List<Flowable<DistanceResponse>> flowableDistance, final List<String> listOrderId) {
        mPosition = 0;
        Flowable.concat(flowableDistance)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(new IoScheduler())
                .subscribe(new DisposableSubscriber<DistanceResponse>() {
                    @Override
                    public void onNext(DistanceResponse distanceResponse) {
                        Log.d("updateLocation", " getDistance2Location_onNext " + distanceResponse.getStatus());
                        getIFace().getDistance2LocationSuccess(distanceResponse, listOrderId.get(mPosition));
                        mPosition = mPosition + 1;
                    }

                    @Override
                    public void onError(Throwable t) {
                        getIFace().getDistance2LocationFail(t.hashCode());
                        mPosition = mPosition + 1;
                        Log.d("updateLocation", " getDistance2Location_onError " + mPosition);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
