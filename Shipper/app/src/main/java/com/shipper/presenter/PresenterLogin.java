package com.shipper.presenter;

import com.shipper.model.net.DataCallBack;
import com.shipper.model.repository.LoginRepository;
import com.shipper.model.response.Login;
import com.shipper.view.iface.IActivityLogin;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class PresenterLogin extends PresenterBase<IActivityLogin> {
    private LoginRepository loginRepository;

    @Override
    public void onInit() {
        super.onInit();
        loginRepository = LoginRepository.getInstance(getIFace().getContext());
    }

    public void login(String userName, String password) {
        getIFace().showLoading();
        loginRepository.login(userName, password, new DataCallBack<Login>() {
            @Override
            public void onSuccess(Login result) {
                getIFace().loginSuccess(result);
                getIFace().hideLoading();
            }

            @Override
            public void onError(int errorCode) {
                getIFace().loginFail(errorCode);
                getIFace().hideLoading();
            }
        });
    }
}
