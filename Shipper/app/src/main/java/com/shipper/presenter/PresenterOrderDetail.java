package com.shipper.presenter;

import com.shipper.model.net.DataCallBack;
import com.shipper.model.repository.OrderDetailRepository;
import com.shipper.model.response.Order;
import com.shipper.view.iface.IFragmentListOrderDetail;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class PresenterOrderDetail extends PresenterBase<IFragmentListOrderDetail> {
    private OrderDetailRepository orderDetailRepository;

    @Override
    public void onInit() {
        super.onInit();
        orderDetailRepository = OrderDetailRepository.getInstance(getIFace().getContext());
    }

    public void getListOrderDetail(String orderId) {
        getIFace().showLoading();
        orderDetailRepository.getListOrderDetail(orderId, new DataCallBack<Order>() {
            @Override
            public void onSuccess(Order result) {
                getIFace().hideLoading();
                getIFace().getListOrderDetailSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().getListOrderDetailFail(errorCode);
                getIFace().hideLoading();
            }
        });
    }

    public void doConfirmShipping(String status, String orderId) {
        getIFace().showLoading();
        orderDetailRepository.doConfirmShipping(status, orderId, new DataCallBack<Order>() {
            @Override
            public void onSuccess(Order result) {
                getIFace().hideLoading();
                getIFace().doConfirmShippingSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().getListOrderDetailFail(errorCode);
                getIFace().hideLoading();
            }
        });
    }
}
