package com.shipper.global;

/**
 * Created by PhuocDH on 8/24/17.
 */

public class ConstantDefine {
    // Code Server return
    public static final int CODE_SUCCESS = 200;
    public static final int INVALIDATE_TOKEN = 403;
    public static final String MSG_ERROR = "Có lỗi trong quá trình xử lý !!!";
}
