package com.shipper.global;

/**
 * Created by PhuocDH on 11/2/2017.
 * Define call request api
 */

public class GlobalDefine {
    //Common code Request
    public static final int REQUEST_CODE_FAIL = 0;
    public static final String COMPLETED = "completed";
    public static final String PROCESSING = "processing";
    public static final String PENDING = "pending";
    public static final String SHIPPER_ON_HAND = "shipper-on-hand";
    public static final String CUSTOMER_ON_HAND = "customer-on-hand";
    public static final String KEEPER_ON_HAND = "keeper-on-hand";
    public static final String ON_HOLD = "on-hold";
    public static final String ROLE_ADMIMN = "shop_manager";
    public static final String ROLE_SHIPPER = "Subcriber";

}


