package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/28/2017.
 */

public class Data extends BaseModel {
    @SerializedName("status")
    private int status;

    public int getStatus() {
        return status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.status);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.status = in.readInt();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
