package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PhuocDh on 11/4/2017.
 */

public class ShippingLine extends BaseModel {

    @SerializedName("id")
    private Long id;
    @SerializedName("method_title")
    private String methodTitle;
    @SerializedName("method_id")
    private String methodId;
    @SerializedName("total")
    private String total;
    @SerializedName("total_tax")
    private String totalTax;
    @SerializedName("taxes")
    private List<Object> taxes;
    @SerializedName("meta_data")
    private List<MetaData> metaData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMethodTitle() {
        return methodTitle;
    }

    public void setMethodTitle(String methodTitle) {
        this.methodTitle = methodTitle;
    }

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public List<Object> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Object> taxes) {
        this.taxes = taxes;
    }

    public List<MetaData> getMetaData() {
        return metaData;
    }

    public void setMetaData(List<MetaData> metaData) {
        this.metaData = metaData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.methodTitle);
        dest.writeString(this.methodId);
        dest.writeString(this.total);
        dest.writeString(this.totalTax);
        dest.writeList(this.taxes);
        dest.writeList(this.metaData);
    }

    public ShippingLine() {
    }

    protected ShippingLine(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.methodTitle = in.readString();
        this.methodId = in.readString();
        this.total = in.readString();
        this.totalTax = in.readString();
        this.taxes = new ArrayList<Object>();
        in.readList(this.taxes, Object.class.getClassLoader());
        this.metaData = new ArrayList<MetaData>();
        in.readList(this.metaData, MetaData.class.getClassLoader());
    }

    public static final Creator<ShippingLine> CREATOR = new Creator<ShippingLine>() {
        @Override
        public ShippingLine createFromParcel(Parcel source) {
            return new ShippingLine(source);
        }

        @Override
        public ShippingLine[] newArray(int size) {
            return new ShippingLine[size];
        }
    };
}
