package com.shipper.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class Geometry {
    @SerializedName("location")
    private GeometryLocation geometryLocation;

    public GeometryLocation getGeometryLocation() {
        return geometryLocation;
    }

    public void setGeometryLocation(GeometryLocation geometryLocation) {
        this.geometryLocation = geometryLocation;
    }
}
