package com.shipper.model.response;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class BaseResponse {

    private int code;
    private String message;
    private String status;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
