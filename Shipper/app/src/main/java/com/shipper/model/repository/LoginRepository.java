package com.shipper.model.repository;

import android.content.Context;

import com.shipper.global.ConstantDefine;
import com.shipper.global.GlobalDefine;
import com.shipper.model.net.DataCallBack;
import com.shipper.model.net.NetworkUtils;
import com.shipper.model.net.ServerPath;
import com.shipper.model.response.Login;
import com.shipper.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by PhuocDH on 9/7/2017.
 */

public class LoginRepository {
    private static LoginRepository mInstance;
    private Context context;

    /**
     * @param context
     */
    private LoginRepository(Context context) {
        mInstance = this;
        this.context = context;
    }

    /**
     * @param context
     * @return
     */
    public static synchronized LoginRepository getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new LoginRepository(context);
        }
        return mInstance;
    }

    /**
     * @param userName
     * @param password
     * @param callBack
     */
    public void login(String userName, String password, final DataCallBack<Login> callBack) {
        Map<String, String> login = new HashMap<>();
        login.put("username", userName);
        login.put("password", password);
        Call<Login> call = NetworkUtils.getInstance(context).getRetrofitService(true).
                doLogin(ServerPath.API_LOGIN, login);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }
}
