package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class Duration extends BaseModel {
    @SerializedName("text")
    private String text;
    @SerializedName("value")
    private int value;


    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeInt(this.value);
    }

    public Duration() {
    }

    protected Duration(Parcel in) {
        this.text = in.readString();
        this.value = in.readInt();
    }

    public static final Creator<Duration> CREATOR = new Creator<Duration>() {
        @Override
        public Duration createFromParcel(Parcel source) {
            return new Duration(source);
        }

        @Override
        public Duration[] newArray(int size) {
            return new Duration[size];
        }
    };
}
