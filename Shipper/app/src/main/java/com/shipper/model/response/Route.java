package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class Route extends BaseModel {

    @SerializedName("legs")
    private List<Leg> legs = null;


    public List<Leg> getLegs() {
        return legs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.legs);
    }

    public Route() {
    }

    protected Route(Parcel in) {
        this.legs = in.createTypedArrayList(Leg.CREATOR);
    }

    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel source) {
            return new Route(source);
        }

        @Override
        public Route[] newArray(int size) {
            return new Route[size];
        }
    };
}
