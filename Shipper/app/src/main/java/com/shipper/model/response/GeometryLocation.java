package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class GeometryLocation extends BaseModel {

    @SerializedName("lat")
    private Double lat = 0.0;
    @SerializedName("lng")
    private Double lng = 0.0;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public GeometryLocation() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.lat);
        dest.writeValue(this.lng);
    }

    protected GeometryLocation(Parcel in) {
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<GeometryLocation> CREATOR = new Creator<GeometryLocation>() {
        @Override
        public GeometryLocation createFromParcel(Parcel source) {
            return new GeometryLocation(source);
        }

        @Override
        public GeometryLocation[] newArray(int size) {
            return new GeometryLocation[size];
        }
    };
}
