package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class Location extends BaseModel  {

    @SerializedName("formatted_address")
    private String formatted_address;
    @SerializedName("geometry")
    private Geometry geometry;
    @SerializedName("icon")
    private String icon;
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("place_id")
    private String placeId;
    @SerializedName("reference")
    private String reference;
    @SerializedName("url")
    private String url;
    @SerializedName("utc_offset")
    private int utcOffset;

    public String getFormatted_address() {
        return formatted_address;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public String getIcon() {
        return icon;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getReference() {
        return reference;
    }

    public String getUrl() {
        return url;
    }

    public int getUtcOffset() {
        return utcOffset;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.formatted_address);
    }

    public Location() {
    }

    protected Location(Parcel in) {
        this.formatted_address = in.readString();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel source) {
            return new Location(source);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };
}
