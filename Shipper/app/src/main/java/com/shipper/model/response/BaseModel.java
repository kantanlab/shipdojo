package com.shipper.model.response;

import android.os.Parcelable;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public abstract class BaseModel implements Parcelable {
    public String toString() {
        return this.getClass().getName();
    }
}
