package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class Distance extends BaseModel {

    @SerializedName("text")
    private String text;
    @SerializedName("value")
    private long value;

    public String getText() {
        return text;
    }

    public long getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeValue(this.value);
    }

    public Distance() {
    }

    protected Distance(Parcel in) {
        this.text = in.readString();
        this.value = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Distance> CREATOR = new Creator<Distance>() {
        @Override
        public Distance createFromParcel(Parcel source) {
            return new Distance(source);
        }

        @Override
        public Distance[] newArray(int size) {
            return new Distance[size];
        }
    };
}
