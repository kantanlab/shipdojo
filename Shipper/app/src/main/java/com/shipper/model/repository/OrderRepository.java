package com.shipper.model.repository;

import android.content.Context;

import com.shipper.global.GlobalDefine;
import com.shipper.model.net.DataCallBack;
import com.shipper.model.net.NetworkUtils;
import com.shipper.model.net.ServerPath;
import com.shipper.model.response.DistanceResponse;
import com.shipper.model.response.LocationResponse;
import com.shipper.model.response.Order;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by PhuocDH on 9/7/2017.
 */

public class OrderRepository {
    private static OrderRepository mInstance;
    private Context context;

    /**
     * @param context
     */
    private OrderRepository(Context context) {
        mInstance = this;
        this.context = context;
    }

    /**
     * @param context
     * @return
     */
    public static synchronized OrderRepository getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new OrderRepository(context);
        }
        return mInstance;
    }

    /**
     * @param callBack
     */
    public void getListOrder(final DataCallBack<List<Order>> callBack) {
        Call<List<Order>> call = NetworkUtils.getInstance(context).getRetrofitService(false).
                getListOrder(ServerPath.API_LIST_ORDER);
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
                }
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }

    /**
     * @param address
     */
    public Flowable<LocationResponse> getLocation(String address, String key) {
        return NetworkUtils.getInstance(context).getRetrofitServiceLocation().
                getLocation(ServerPath.API_LOCATION_PLACE, address, key);
    }

    /**
     * @param locationStart
     * @param locationEnd
     */
    public Flowable<DistanceResponse> getDistance2Location(String locationStart, String locationEnd, String key) {
        return NetworkUtils.getInstance(context).getRetrofitServiceLocation().
                getDistance2Location(ServerPath.API_DISTANCE_2_LOCATION, locationStart, locationEnd, false, "metric", "driving", key);
    }
}
