package com.shipper.model.net;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class ServerPath {
    public static final String GOOGLE_MAP_URL = "https://maps.googleapis.com";
    public static final String SERVER_PATH = "https://shopdi.vn/";
    private static final String DOMAIN = "wp-json/wc/v2/";
    private static final String GOOGLE_LOCATION_DOMAIN = "/maps/api/geocode/json";
    private static final String GOOGLE_DISTANCE_DOMAIN = "/maps/api/directions/json";

    // Login
    public static final String API_LOGIN = SERVER_PATH + "wp-json/jwt-auth/v1/token";

    // Get list order
    public static final String API_LIST_ORDER = SERVER_PATH + DOMAIN + "orders/";

    // Validate token
    public static final String API_INVALID_TOKEN = API_LOGIN + "/validate";

    // Map
    public static final String API_LOCATION_PLACE = GOOGLE_MAP_URL + GOOGLE_LOCATION_DOMAIN;
    public static final String API_DISTANCE_2_LOCATION = GOOGLE_MAP_URL + GOOGLE_DISTANCE_DOMAIN;
}
