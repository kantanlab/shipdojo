package com.shipper.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class MetaData {

    @SerializedName("id")
    private Long id;
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
