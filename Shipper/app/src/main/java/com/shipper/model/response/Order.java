package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;
import java.util.List;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class Order extends BaseModel {
    @SerializedName("id")
    private Long id;
    @SerializedName("number")
    private String number;
    @SerializedName("status")
    private String status;
    @SerializedName("currency")
    private String currency;
    @SerializedName("shipping_total")
    private String shippingTotal;
    @SerializedName("total")
    private String total;
    @SerializedName("customer_note")
    private String customerNote;
    @SerializedName("billing")
    private Billing billing;
    @SerializedName("shipping")
    private Shipping shipping;
    @SerializedName("payment_method")
    private String paymentMethod;
    @SerializedName("payment_method_title")
    private String paymentMethodTitle;
    @SerializedName("line_items")
    private List<LineItem> lineItems;
    @SerializedName("shipping_lines")
    private List<ShippingLine> shippingLines;
    private GeometryLocation geometryLocationEnd;
    private GeometryLocation geometryLocationCurrent;
    private String distance;
    private long distanceValue;
    private String duration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShippingTotal() {
        return shippingTotal;
    }

    public void setShippingTotal(String shippingTotal) {
        this.shippingTotal = shippingTotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCustomerNote() {
        return customerNote;
    }

    public void setCustomerNote(String customerNote) {
        this.customerNote = customerNote;
    }

    public Billing getBilling() {
        return billing;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodTitle() {
        return paymentMethodTitle;
    }

    public void setPaymentMethodTitle(String paymentMethodTitle) {
        this.paymentMethodTitle = paymentMethodTitle;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public List<ShippingLine> getShippingLines() {
        return shippingLines;
    }

    public void setShippingLines(List<ShippingLine> shippingLines) {
        this.shippingLines = shippingLines;
    }

    public GeometryLocation getGeometryLocationEnd() {
        return geometryLocationEnd;
    }

    public void setGeometryLocationEnd(GeometryLocation geometryLocationEnd) {
        this.geometryLocationEnd = geometryLocationEnd;
    }

    public GeometryLocation getGeometryLocationCurrent() {
        return geometryLocationCurrent;
    }

    public void setGeometryLocationCurrent(GeometryLocation geometryLocationCurrent) {
        this.geometryLocationCurrent = geometryLocationCurrent;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public long getDistanceValue() {
        return distanceValue;
    }

    public void setDistanceValue(long distanceValue) {
        this.distanceValue = distanceValue;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Order() {
    }

    // Comparator
    public static class CompDistance implements Comparator<Order> {
        @Override
        public int compare(Order arg0, Order arg1) {
            return (int)arg0.getDistanceValue() - (int)arg1.getDistanceValue();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.number);
        dest.writeString(this.status);
        dest.writeString(this.currency);
        dest.writeString(this.shippingTotal);
        dest.writeString(this.total);
        dest.writeString(this.customerNote);
        dest.writeParcelable(this.billing, flags);
        dest.writeParcelable(this.shipping, flags);
        dest.writeString(this.paymentMethod);
        dest.writeString(this.paymentMethodTitle);
        dest.writeTypedList(this.lineItems);
        dest.writeTypedList(this.shippingLines);
        dest.writeParcelable(this.geometryLocationEnd, flags);
        dest.writeParcelable(this.geometryLocationCurrent, flags);
        dest.writeString(this.distance);
        dest.writeLong(this.distanceValue);
        dest.writeString(this.duration);
    }

    protected Order(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.number = in.readString();
        this.status = in.readString();
        this.currency = in.readString();
        this.shippingTotal = in.readString();
        this.total = in.readString();
        this.customerNote = in.readString();
        this.billing = in.readParcelable(Billing.class.getClassLoader());
        this.shipping = in.readParcelable(Shipping.class.getClassLoader());
        this.paymentMethod = in.readString();
        this.paymentMethodTitle = in.readString();
        this.lineItems = in.createTypedArrayList(LineItem.CREATOR);
        this.shippingLines = in.createTypedArrayList(ShippingLine.CREATOR);
        this.geometryLocationEnd = in.readParcelable(GeometryLocation.class.getClassLoader());
        this.geometryLocationCurrent = in.readParcelable(GeometryLocation.class.getClassLoader());
        this.distance = in.readString();
        this.distanceValue = in.readLong();
        this.duration = in.readString();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
