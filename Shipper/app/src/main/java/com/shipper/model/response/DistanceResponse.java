package com.shipper.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class DistanceResponse extends  BaseResponse {

    @SerializedName("routes")
    private List<Route> routes = null;

    public List<Route> getRoutes() {
        return routes;
    }

}
