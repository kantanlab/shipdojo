package com.shipper.model.net;

import com.shipper.model.response.Data;
import com.shipper.model.response.DistanceResponse;
import com.shipper.model.response.LocationResponse;
import com.shipper.model.response.Login;
import com.shipper.model.response.Order;

import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by PhuocDH on 11/2/2017.
 * Define call request api
 */

public interface ServerRequest {

    @POST
    Call<Data> invalidToken(@Url String url);

    @POST
    Call<Login> doLogin(@Url String url,
                        @Body Map<String, String> login);

    @GET
    Call<List<Order>> getListOrder(@Url String url);

    @GET
    Call<Order> getListOrderDetail(@Url String url);

    @POST
    Call<Order> doConfirmShipping(@Url String url, @Body Map<String, String> status);

    @GET
    Flowable<LocationResponse> getLocation(@Url String url,
                                           @Query("address") String address,
                                           @Query("key") String key);

    @GET
    Flowable<DistanceResponse> getDistance2Location(@Url String url,
                                                    @Query("origin") String origin,
                                                    @Query("destination") String destination,
                                                    @Query("sensor") boolean sensor,
                                                    @Query("units") String units,
                                                    @Query("mode") String mode,
                                                    @Query("key") String key);
}
