package com.shipper.model.net;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public abstract class DataCallBack<T> {

    public abstract void onSuccess(T result);

    public abstract void onError(int errorCode);

}