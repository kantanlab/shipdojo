package com.shipper.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PhuocDH on 11/12/2017.
 */

public class LocationResponse extends BaseResponse {

    @SerializedName("results")
    private List<Location> location;

    public List<Location> getLocation() {
        return location;
    }
}
