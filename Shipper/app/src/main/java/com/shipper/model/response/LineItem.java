package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PhuocDH on 11/4/2017.
 */

public class LineItem extends BaseModel {

    @SerializedName("id")
    private Long id;
    @SerializedName("name")
    private String name;
    @SerializedName("product_id")
    private Long productId;
    @SerializedName("variation_id")
    private Long variationId;
    @SerializedName("quantity")
    private Long quantity;
    @SerializedName("tax_class")
    private String taxClass;
    @SerializedName("subtotal")
    private String subtotal;
    @SerializedName("subtotal_tax")
    private String subtotalTax;
    @SerializedName("total")
    private String total;
    @SerializedName("total_tax")
    private String totalTax;
    @SerializedName("taxes")
    private List<Object> taxes;
    @SerializedName("meta_data")
    private List<MetaData> metaData;
    @SerializedName("sku")
    private String sku;
    @SerializedName("price")
    private Long price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getVariationId() {
        return variationId;
    }

    public void setVariationId(Long variationId) {
        this.variationId = variationId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotalTax() {
        return subtotalTax;
    }

    public void setSubtotalTax(String subtotalTax) {
        this.subtotalTax = subtotalTax;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public List<Object> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Object> taxes) {
        this.taxes = taxes;
    }

    public List<MetaData> getMetaData() {
        return metaData;
    }

    public void setMetaData(List<MetaData> metaData) {
        this.metaData = metaData;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeValue(this.productId);
        dest.writeValue(this.variationId);
        dest.writeValue(this.quantity);
        dest.writeString(this.taxClass);
        dest.writeString(this.subtotal);
        dest.writeString(this.subtotalTax);
        dest.writeString(this.total);
        dest.writeString(this.totalTax);
        dest.writeList(this.taxes);
        dest.writeList(this.metaData);
        dest.writeString(this.sku);
        dest.writeValue(this.price);
    }

    public LineItem() {
    }

    protected LineItem(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.productId = (Long) in.readValue(Long.class.getClassLoader());
        this.variationId = (Long) in.readValue(Long.class.getClassLoader());
        this.quantity = (Long) in.readValue(Long.class.getClassLoader());
        this.taxClass = in.readString();
        this.subtotal = in.readString();
        this.subtotalTax = in.readString();
        this.total = in.readString();
        this.totalTax = in.readString();
        this.taxes = new ArrayList<Object>();
        in.readList(this.taxes, Object.class.getClassLoader());
        this.metaData = new ArrayList<MetaData>();
        in.readList(this.metaData, MetaData.class.getClassLoader());
        this.sku = in.readString();
        this.price = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Creator<LineItem> CREATOR = new Creator<LineItem>() {
        @Override
        public LineItem createFromParcel(Parcel source) {
            return new LineItem(source);
        }

        @Override
        public LineItem[] newArray(int size) {
            return new LineItem[size];
        }
    };
}
