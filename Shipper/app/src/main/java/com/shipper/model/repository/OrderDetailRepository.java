package com.shipper.model.repository;

import android.content.Context;

import com.shipper.global.GlobalDefine;
import com.shipper.model.net.DataCallBack;
import com.shipper.model.net.NetworkUtils;
import com.shipper.model.net.ServerPath;
import com.shipper.model.response.Order;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by PhuocDH on 9/7/2017.
 */

public class OrderDetailRepository {
    private static OrderDetailRepository mInstance;
    private Context context;

    /**
     * @param context
     */
    private OrderDetailRepository(Context context) {
        mInstance = this;
        this.context = context;
    }

    /**
     * @param context
     * @return
     */
    public static synchronized OrderDetailRepository getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new OrderDetailRepository(context);
        }
        return mInstance;
    }

    /**
     * @param orderId
     * @param callBack
     */
    public void getListOrderDetail(String orderId, final DataCallBack<Order> callBack) {
        Call<Order> call = NetworkUtils.getInstance(context).getRetrofitService(false).
                getListOrderDetail(ServerPath.API_LIST_ORDER + orderId);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }

    /**
     * @param status
     * @param orderId
     * @param callBack
     */
    public void doConfirmShipping(String status, String orderId, final DataCallBack<Order> callBack) {
        Map<String, String> map = new HashMap<>();
        map.put("status", status);
        Call<Order> call = NetworkUtils.getInstance(context).getRetrofitService(false).
                doConfirmShipping(ServerPath.API_LIST_ORDER + orderId, map);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }
}
