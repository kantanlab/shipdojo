package com.shipper.model.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class Login extends BaseModel {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("token")
    private String token;
    @SerializedName("user_email")
    private String userEmail;
    @SerializedName("user_nicename")
    private String userNicename;
    @SerializedName("user_display_name")
    private String userDisplayName;
    @SerializedName("user_roles")
    private List<String> userRoles = new ArrayList<>();
    private boolean isRemember = false;
    private String apiLocationKey = "AIzaSyAqB5BZN_ckWS6nfHTgtnCQ-36ixrp4q68";

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public String getToken() {
        return token;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserNicename() {
        return userNicename;
    }

    public String getUserDisplayName() {
        return userDisplayName;
    }

    public List<String> getUserRoles() {
        return userRoles;
    }

    public boolean isRemember() {
        return isRemember;
    }

    public void setRemember(boolean remember) {
        isRemember = remember;
    }

    public String getApiLocationKey() {
        return apiLocationKey;
    }

    public Login() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.message);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.token);
        dest.writeString(this.userEmail);
        dest.writeString(this.userNicename);
        dest.writeString(this.userDisplayName);
        dest.writeStringList(this.userRoles);
        dest.writeByte(this.isRemember ? (byte) 1 : (byte) 0);
    }

    protected Login(Parcel in) {
        this.code = in.readString();
        this.message = in.readString();
        this.data = in.readParcelable(Data.class.getClassLoader());
        this.token = in.readString();
        this.userEmail = in.readString();
        this.userNicename = in.readString();
        this.userDisplayName = in.readString();
        this.userRoles = in.createStringArrayList();
        this.isRemember = in.readByte() != 0;
    }

    public static final Creator<Login> CREATOR = new Creator<Login>() {
        @Override
        public Login createFromParcel(Parcel source) {
            return new Login(source);
        }

        @Override
        public Login[] newArray(int size) {
            return new Login[size];
        }
    };
}
