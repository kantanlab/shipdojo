package com.shipper.model.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Credentials;
import android.net.NetworkInfo;

import com.shipper.global.ConstantDefine;
import com.shipper.global.GlobalDefine;
import com.shipper.model.response.Data;
import com.shipper.model.response.Login;
import com.shipper.utils.SharedPreferencesUtils;
import com.shipper.utils.Utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by PhuocDH on 11/2/2017.
 */

public class NetworkUtils {
    private static final long TIME_OUT = 20000;
    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTHORIZATION_TYPE = "Dojo ";
    private static NetworkUtils mInstance;
    private Context context;
    private Retrofit retrofit;
    private Retrofit retrofitMap;
    private ServerRequest service;
    private ServerRequest serviceMap;
    private boolean isLoginTmp;

    public NetworkUtils() {
    }

    public static synchronized NetworkUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkUtils();
            mInstance.setContext(context);
        }
        return mInstance;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ServerRequest getRetrofitService(boolean isLogin) {
        // Init OkHttpClient
         isLoginTmp = isLogin;
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient().newBuilder();
        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.readTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder();
                builder.addHeader("Content-Type", "application/json");
                if (!isLoginTmp) {
                    String authToken = SharedPreferencesUtils.getInstance(getContext()).getUserProfile().getToken();
                    builder.addHeader(AUTHORIZATION, AUTHORIZATION_TYPE + authToken);
                }
                Request request = builder.build();
                return chain.proceed(request);
            }
        });
        if (null == retrofit) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ServerPath.SERVER_PATH)
                    .client(okHttpBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (null == service)
            service = retrofit.create(ServerRequest.class);

        if (!isLoginTmp) {
            service.invalidToken(ServerPath.API_INVALID_TOKEN).enqueue(new Callback<Data>() {
                @Override
                public void onResponse(Call<Data> call, retrofit2.Response<Data> response) {
                    if (null != response.body() && response.body().getStatus() == ConstantDefine.INVALIDATE_TOKEN) {
                        Utils.invalidToken(getContext());
                        return;
                    }
                }

                @Override
                public void onFailure(Call<Data> call, Throwable t) {

                }
            });
        }
        return service;
    }

    public ServerRequest getRetrofitServiceLocation() {
        // Init OkHttpClient
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient().newBuilder();
        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.readTimeout(TIME_OUT, TimeUnit.MILLISECONDS);

        if (null == retrofitMap) {
            retrofitMap = new Retrofit.Builder()
                    .baseUrl(ServerPath.GOOGLE_MAP_URL)
                    .client(okHttpBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        if (null == serviceMap)
            serviceMap = retrofitMap.create(ServerRequest.class);
        return serviceMap;
    }
}
